CC = gcc
FLAGS = -Wall -g
SRC_DIR = src
BLD_DIR = bld
EXEC = a.out
IN_DIR = in
IN = in.txt
OUT_DIR = out
OUT = out.txt

all : $(BLD_DIR)/$(EXEC)

$(BLD_DIR)/$(EXEC) : $(BLD_DIR)/copy.o
	$(CC) $(FLAGS) -o $@ $^

$(BLD_DIR)/copy.o : $(SRC_DIR)/copy.h $(SRC_DIR)/copy.c
	$(CC) $(FLAGS) -o $@ -c $(word 2, $^)

test : $(BLD_DIR)/$(EXEC)
	./$< $(IN_DIR) $(OUT_DIR)

clean :
	rm -rf $(BLD_DIR)/*
	rm -rf $(OUT_DIR)/*
	rm -rf $(OUT)

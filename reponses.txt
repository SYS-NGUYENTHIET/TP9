Exercice 1
----------

1)
nombre de pointeur par bloc : 1K / 4 = 2^(10-2) = 2^8 = 256

12Ki + 256Ki + 256^2Ki + 256^3Ki = 16 843 020Ki = 16Gi

12K -> blocs adressés par l'i-noeud
256K -> blocs adressés par le pointeur indirect simple
256K^2 -> blocs adressés par le pointeur indirect double
256K^3 -> blocs adressés par le pointeur indirect triple

2)
100 000 / 1K = 98

On a besoin de 98 blocs pour stocker les données du fichier

L'i-noeud nous permet d'adresser 12 blocs, il reste 98 - 12 = 86 blocs à adresser.

Le pointeur indirect simple nous permet d'adresser en plus 256 blocs et faisant perdre 1 bloc. Il ne reste plus de blocs à adresser.

Le nombre total de bloc de données pour représenter ce fichier est égal à 1 + 98 = 99 blocs.

TP10
----

Exercice 3
----------

2) la taille des blocs est importante car elle influence sur le nombre de page que la MMU va charger en mémoire

3) Jusqu'a 1k, le temps nécessaire augmentaient, mais à partir de 1k le temps necessaire diminuaient. Cela est du au fait qu'avant 1k, on avait besoin de charger
en mémoire les pages plus souvent, alors qu'apres 1k, on chargaient les pages en mémoires moins souvent
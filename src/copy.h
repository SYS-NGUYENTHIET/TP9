#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>

#define BUFFER_SIZE 67108864 
#define MAX_PATH_SIZE 4096
#define MAX_FILENAME_SIZE 255

int copyd(const char *, const char *);
int create_path(const char *, const char *, char[MAX_PATH_SIZE]);
int copyf(const char *, const char *);
int copyftod(const char *, const char *);
int get_file_name(const char *, char[MAX_FILENAME_SIZE]);
int copy_fun_read_write(int, int);
int copy_fun_mmap(int, int);

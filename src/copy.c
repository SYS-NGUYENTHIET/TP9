#include "copy.h"

// copy a dir entries ( except . and .. ) to another
// return 0 upon success
// return 1 upon failure
// return -1 if both parameters are files
int copyd(const char *src, const char *dst)
{
    // a buffer to check meta data of file
    struct stat tmp_stat;

    // first, we verify if sources AND destination are directories
    DIR *src_dir = opendir(src);
    DIR *dst_dir = opendir(dst);

    // the buffer for full file path of the destination files, the maximum length of a path is 4096 according to this Stackoveflow post :
    // https://unix.stackexchange.com/questions/32795/what-is-the-maximum-allowed-filename-and-folder-size-with-ecryptfs
    char *output_path = (char *)malloc(sizeof(char) * MAX_PATH_SIZE);

    // buffer for source files
    char *input_path = (char *)malloc(sizeof(char) * MAX_PATH_SIZE);

    if (src_dir == NULL)
    {
        // if both directories does not exist, this could mean we are copy files, the copy file function takes over
        if (dst_dir == NULL)
        {
            return -1;
        }
        // special case when source is not a directory, but desination is a directory
        else
        {
            // verify if src exists
            if (stat(src, &tmp_stat) == 0)
            {
                // if src is a file, special case where we copy file to dir
                if (S_ISREG(tmp_stat.st_mode))
                {
                    int res = copyftod(src, dst);
                    closedir(dst_dir);
                    closedir(src_dir);
                    return res;
                }

                // we don't take into account anything that isn't a file or directory
                else
                {
                    printf("Error : %s is neither a file or directory\n", src);
                    return 1;
                }
            }
            // the file does not exist, it is an error
            else
            {
                printf("Error : %s does not exist\n", src);
                return 1;
            }
        }
    }
    else
    {
        // the destination directory does not exist, we create it
        if (dst_dir == NULL)
        {
            // first we check if the destination isn't a file, if it is, it is an error
            if (stat(dst, &tmp_stat) == 0 && S_ISREG(tmp_stat.st_mode))
            {
                return 1;
            }
            else
            {
                // get the rights of the source dir, we know from previous statement that src is a dir
                stat(src, &tmp_stat);

                // create the new directory
                if (mkdir(dst, tmp_stat.st_mode) != 0)
                {
                    // return error in case directory couldn't be created
                    return 1;
                }
            }
        }
        // both source and destination directories exist, we can copy over each file contained
        // in the source destination

        // the file name buffer, 256 is the maximum length of a filename according to man readdir
        char *file_name = (char *)malloc(sizeof(char) * MAX_FILENAME_SIZE);

        // get the next file in the directory
        struct dirent *next_file = readdir(src_dir);

        // the result of copying files, should stay 0 if everything was successful
        int res = 0;

        while (next_file != NULL)
        {
            // copying the file name into the buffer
            strncpy(file_name, next_file->d_name, MAX_FILENAME_SIZE * sizeof(char));

            // omitting entries . and .., for safety we will only compare the first characters
            if (!(strncmp(".", file_name, 2) == 0 || strncmp("..", file_name, 3) == 0))
            {

                // create the outputh path, if unsuccesful we stop
                if (create_path(dst, file_name, output_path) != 0 || create_path(src, file_name, input_path) != 0)
                {
                    return 1;
                }

                // otherwise, we copy the file, the copy file function takes over
                else
                {
                    if (stat(input_path, &tmp_stat) == 0)
                    {
                        // if the file is a dir, we recurse and try to copy the dir instead
                        if (S_ISDIR(tmp_stat.st_mode))
                        {
                            res += copyd(input_path, output_path);
                        }
                        // if not, we copy the file as usual procedure
                        else
                        {
                            res += copyf(input_path, output_path);
                        }
                    }
                    else
                    {
                        return 1;
                    }
                }

                // reset the paths
                strcpy(output_path, "");
                strcpy(input_path, "");
            }

            // read the next file
            next_file = readdir(src_dir);
        }

        // free the memory
        free((void *)file_name);
        free((void *)input_path);
        free((void *)output_path);

        // close the dir
        closedir(src_dir);
        closedir(dst_dir);

        return res;
    }
}

// create a path and put it in buffer, return 0 upon success, 1 upon failure
int create_path(const char *dst, const char *file_name, char buffer[MAX_PATH_SIZE])
{

    // length of the destination directory path
    int path_len = strlen(dst);

    // length of the destination file path
    int name_len = strlen(file_name);

    // whether or not a / has been added to the output path
    int slash_added;

    // if bigger than 4096, it is an error
    if (path_len + name_len > MAX_PATH_SIZE)
    {
        printf("destination path too big\n");
        return 1;
    }
    else
    {
        // at this point, it is safe to use the length of both dst and file_name because they
        // won't overflow the buffer
        strncpy(buffer, dst, path_len * sizeof(char));

        // if the directory path doesn't contain /, we add it
        if (dst[path_len - 1] != '/')
        {
            buffer[path_len] = '/';
            slash_added = 1;
        }
        else
        {
            slash_added = 0;
        }

        strncpy(buffer + path_len + slash_added, file_name, name_len);

        // add the end of string
        buffer[path_len + slash_added + name_len] = '\0';

        return 0;
    }
}

// copy files
// return 0 upon succes, 1 upon failure
int copyf(const char *src, const char *dst)
{
    // attempt to copy directories first
    int copy_dir_attempt = copyd(src, dst);

    // if copy of dir was either succesful or failed, return the result
    if (copy_dir_attempt >= 0)
    {
        return copy_dir_attempt;
    }
    // otherwise, we are copying files
    else
    {
        // open the files
        int src_desc = open(src, O_RDONLY);
        int dst_desc = open(dst, O_WRONLY | O_CREAT | O_EXCL);

        // file does not exist
        if (src_desc < 0)
        {
            printf("source file not found\n");
            return 1;
        }
        else
        {
            if (dst_desc < 0)
            {
                printf("destination file already exists\n");
                return 1;
            }
            else
            {
                // if (copy_fun_read_write(src_desc, dst_desc) == 1)
                if (copy_fun_mmap(src_desc, dst_desc) == 1)
                {
                    return 1;
                }
            }
        }

        // close the files
        int a = close(src_desc);
        int b = close(dst_desc);

        // on success should be 0
        return a + b;
    }
}

int copy_fun_mmap(int src_desc, int dst_desc)
{
    struct stat metadata;
    if (fstat(src_desc, &metadata) < 0)
    {
        return 1;
    }

    char *addr = mmap(NULL, metadata.st_size, PROT_READ, MAP_SHARED, src_desc, 0);
    if (addr == MAP_FAILED)
    {
        printf("fail map\n");
        return 1;
    }

    if (write(dst_desc, addr, metadata.st_size) == 1)
    {
        printf("fail write\n");
        return 1;
    }
    if (munmap(addr, metadata.st_size) != 0)
    {
        printf("fail unmap\n");
        return 1;
    }

    fchmod(dst_desc, metadata.st_mode);
    return 0;
}

int copy_fun_read_write(int src_desc, int dst_desc)
{
    // first we copy over the data to the output
    // create the buffer
    char *buffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);

    // n indicates the number of bytes read
    int n = read(src_desc, buffer, BUFFER_SIZE * sizeof(char));

    // loop through the source file
    while (n != 0)
    {
        if (write(dst_desc, buffer, n * sizeof(char)) == -1)
        {
            return 1;
        }
        n = read(src_desc, buffer, sizeof(buffer));
    }

    // secondly, we copy over the read, write, execute rights
    struct stat metadata;

    // m indicates the success of the fstat operation
    if (fstat(src_desc, &metadata) < 0)
    {
        // an error has occured
        return 1;
    }
    else
    {
        // retrieve the source file permission
        mode_t permission = metadata.st_mode;

        // copy the permission over to the destination file
        fchmod(dst_desc, permission);
    }
    free(buffer);
    return 0;
}

// copy a file to a directory
int copyftod(const char *src, const char *dst)
{
    char *new_dst = (char *)malloc(sizeof(char) * MAX_PATH_SIZE);
    char *file_name = (char *)malloc(sizeof(char) * MAX_FILENAME_SIZE);
    if (get_file_name(src, file_name) != 0)
    {
    }
    create_path(dst, file_name, new_dst);
    free(file_name);
    int res = copyf(src, new_dst);
    free(new_dst);
    return res;
}

// put the file name of a path in the buffer
int get_file_name(const char *s, char buffer[MAX_FILENAME_SIZE])
{
    int i = 0, j = 0; // i : index of s, j : index of buffer

    while (s[i] != '\0')
    {
        if (j >= (MAX_FILENAME_SIZE)-1)
        {
            return 1;
        }
        else
        {
            if (s[i] == '/')
            {
                strcpy(buffer, "");
                j = 0;
            }
            else
            {
                buffer[j] = s[i];
                j++;
            }
            i++;
        }
    }
    buffer[j] = '\0';
    return 0;
}

int main(int argc, const char *argv[])
{
    if (argc != 3)
    {
        printf("invalid number of arguments");
        return 1;
    }
    else
    {
        return copyf(argv[1], argv[2]);
    }
}
